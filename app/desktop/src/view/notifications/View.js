Ext.define('Client.view.notifications.View',{
	extend: 'Ext.grid.Grid',
	xtype: 'notifications',
	cls: 'notificationsview',
	requires: [],
	controller: {type: 'notifications'},
	viewModel: {type: 'notifications'},
	store: {type: 'notifications'},
  perspectives: ['main'],
	selectable: { mode: 'single' },
	listeners: {
		select: 'onItemSelected'
	},
	columns: [
		{
			text: 'Name',
			dataIndex: 'name',
			width: 100,
			cell: {userCls: 'bold'}
		},
		{text: 'Email',dataIndex: 'email',width: 230},
		{
			text: 'Phone',
			dataIndex: 'phone',
			width: 150
		}
	]
});
