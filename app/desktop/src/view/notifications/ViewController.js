Ext.define('Client.view.notifications.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.notifications',

	onItemSelected: function (sender, record) {
		Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
	},

	onConfirm: function (choice) {
		if (choice === 'yes') {
			//
		}
	}
});
