Ext.define('Client.view.perspective.admin.header.addons.notificationsBtn.ViewController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.perspective.admin.header.addons.notificationsBtn',

  onNotificationsBtn: (button) => {
	  // let view = Ext.Viewport.down('perspective\\.admin');
    // let vm = view.getViewModel();
    // let vc = view.getController();
    let container = button.up('container');
    let vc = container.getController();
    let vm = container.getViewModel();
    vm.getStore('notifications').load();
    let menu = vc.lookup('notificationsMenu');
    menu.showBy(button, 't-b?');
  },

  onNotificationTap(list, location, eOpts) {
    let me = this;
    let rec = location.record;
    let action = rec.get('action');
    let options = rec.get('options');
    switch (action) {
      case NOTIFICATIONS.ACTIONS.REDIRECT_HASH:
        me.redirectTo(options.hash);
        break;
      case NOTIFICATIONS.ACTIONS.REDIRECT_URL:
        window.location.href = options.url;
        break;
    }
    // console.log(list, location, eOpts);
  },

  onPainted() {
    let me = this;
    let vm = me.getViewModel();
    if (vm.get('addedHooks') === false) {
      let notifications = vm.getStore('notifications');
      let Socket = Client.app.getController('Socket');
      if (Socket.socket) {
        Socket.watch('AdminNotifications');
        Socket.socket.on('new-AdminNotification', (notification) => {
          console.log('Got New AdminNotification', notification);
          notifications.insert(0, notification);
        });
      }
      vm.set('addedHooks', true);
    }
  },

  onAdd(store, records, index, eOpts) {
    let me = this;
    let vm = me.getViewModel();
    let newEntries = vm.get('newEntries') + 1;
    vm.set('newEntries', newEntries);
    let notificationsBtn = me.lookup('notificationsBtn');
    notificationsBtn.setBadgeText(`${newEntries}`);
    // Favicon.badge(newEntries);
  },

  onLoad(store, records, success, operation, eOpts) {
    let me = this;
    let vm = me.getViewModel();
    vm.set('newEntries', 0);
    let notificationsBtn = me.lookup('notificationsBtn');
    notificationsBtn.setBadgeText('');
    // Favicon.badge(0);
  },

    // setTimeout(() => {
    //   let perspective = view.up('perspective\\.main');
    //   let mainVM = perspective.getViewModel();
    //   let user = mainVM.get('user');
    //   // let perspective = Ext.Viewport.down('perspective\\.main');
    //   console.log(view, perspective, user);
    // }, 10);
});
