Ext.define('Client.view.perspective.admin.header.addons.notificationsBtn.ViewModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.perspective.admin.header.addons.notificationsBtn',
  data: {
    addedHooks: false,
    apiUrl: '/api/v1',
    newEntries: 0,
  },
  stores: {
    notifications: {
		  autoLoad: true,
      remoteSort: true,
      sorters: [{
        property: 'createdOn',
        direction: 'DESC'
      }],
      listeners: {
		    add: 'onAdd',
        load: 'onLoad',
      },
      model: 'Client.model.perspective.main.Notification',
      proxy: {
			  type: 'rest',
        url: '{apiUrl}/users/me/getnotifications/1',
        reader: {
			    type: 'json',
          rootProperty: 'data'
        }
      }
		}
  }
});
