Ext.define('Client.view.perspective.main.header.addons.notificationsBtn.View', {
  extend: 'Ext.Container',
  controller: {type: 'perspective.main.header.addons.notificationsBtn'},
  viewModel: {type: 'perspective.main.header.addons.notificationsBtn'},
  padding: 5,
  listeners: {
    painted: 'onPainted'
  },
  items: [{
    xtype: 'button',
    ui: 'round raised',
    bind: {
      tooltip: '{"mod-notifications:NOTIFICATIONS":translate}'
    },
    reference: 'notificationsBtn',
    handler: 'onNotificationsBtn',
    iconCls: 'x-fa fa-bell',
    margin: '0 5px 0 0'
  }, {
    xtype: 'menu',
    reference: 'notificationsMenu',
    itemId: 'notificationsMenu',
    maxWidth: 500,
    minWidth: 350,
    anchor: true,
    items: [{
      xtype: 'list',
      reference: 'notificationsList',
      itemId: 'notificationsList',
      margin: 0,
      maxHeight: 500,
      maxWidth: 500,
      minHeight: 30,
      minWidth: 350,
      scrollable: 'vertical',
      itemTpl: [
        '<div>{createdOn:date("Y/m/d H:i:s")}</div>',
        '<div>{content}</div>'
      ],
      listeners: {
        childtap: 'onNotificationTap'
      },
      bind: {
        store: '{notifications}',
      },
      selectable: false,
      itemConfig: {
        xtype: 'simplelistitem',
      },
      plugins: [{
        type: 'listpaging',
      }]
    }]
  }]
});
Client.view.perspective.main.header.addons.notificationsBtn.View.addStatics({
  order: 2
});
