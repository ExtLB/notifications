Ext.define('Client.model.perspective.main.Notification', {
  extend: 'Ext.data.Model',
  requires: [
    'Ext.data.field.Date',
    'Ext.data.proxy.Rest',
    'Ext.data.reader.Json'
  ],
  fields: [{
    name: 'id',
    persist: false
  }, {
    name: 'content'
  }, {
    name: 'action'
  }, {
    name: 'type'
  }, {
    type: 'date',
    name: 'createdOn'
  }]
});
