global.NOTIFICATIONS = {
  TYPES: {
    MAIN: 0,
    ADMIN: 1,
  },
  ACTIONS: {
    NONE: 0,
    REDIRECT_HASH: 1,
    REDIRECT_URL: 2,
  }
};
