const _ = require('lodash');
const async = require('async');
const NotificationTypes = require('./server/models/notification').NotificationTypes;
const log = require('@dosarrest/loopback-component-logger')('mod-notifications/boot');
const cascadeDelete = require('loopback-cascade-delete-mixin/cascade-delete');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let app = me.app;
    let done = me.done;
    let ACL = app.registry.getModelByType('ACL');
    try {
      let User = app.registry.getModelByType('User');

      User.remoteMethod('getnotifications', {
        isStatic: false,
        accepts: [
          {arg: 'req', type: 'object', http: {source: 'req'}},
          {arg: 'type', type: 'number', required: true},
          {arg: 'start', type: 'number', required: true},
          {arg: 'limit', type: 'number', required: true},
        ],
        returns: [
          {arg: 'total', type: 'number'},
          {arg: 'data', type: 'array'},
        ],
        http: {verb: 'get', path: '/getnotifications/:type'}
      });
      User.prototype.getnotifications = _.partial(me.getnotifications, me);

      let AdminNotification = app.registry.getModelByType('AdminNotification');
      User.hasMany(AdminNotification, {as: 'adminNotifications', foreignKey: 'userId'});
      User.nestRemoting('adminNotifications');
      AdminNotification.belongsTo(User, {as: 'user', foreignKey: 'userId'});

      let Notification = app.registry.getModelByType('Notification');
      User.hasMany(Notification, {as: 'notifications', foreignKey: 'userId'});
      // User.nestRemoting('notifications');
      Notification.belongsTo(User, {as: 'user', foreignKey: 'userId'});

      cascadeDelete(User, {
        relations: ["adminNotifications", "notifications"]
      });
      async.series({
        adminNotificationsACL: (cb) => {
          ACL.find({where: {model: User.definition.name, property: '__get__adminNotifications'}}).then((aclItems) => {
            if (aclItems.length === 0) {
              ACL.create({
                model: User.definition.name,
                accessType: 'READ',
                principalType: 'ROLE',
                principalId: '$owner',
                permission: 'ALLOW',
                property: '__get__adminNotifications'
              }).then((aclItem) => {
                cb(null, true);
              }).catch((aclErr) => {
                log.error(aclErr);
                cb(aclErr);
              });
            } else {
              cb(null, true);
            }
          }).catch((aclErr) => {
            log.error(aclErr);
            cb(aclErr);
          });
        },
        userNotificationsACL: (cb) => {
          ACL.findOrCreate({
            model: User.definition.name,
            accessType: 'READ',
            principalType: 'ROLE',
            principalId: '$owner',
            permission: 'ALLOW',
            property: 'getnotifications'
          }).then((aclItem) => {
            cb(null, true);
          }).catch((aclErr) => {
            log.error(aclErr);
            cb(aclErr);
          });
        }
      }, (err, results) => {
        done(null, true);
      });
    } catch (err) {
      log.error(err);
      done(err);
    }
    // let registry = app.registry;
    // let MainNavigation = registry.getModelByType('MainNavigation');
    // MainNavigation.find({where: {routeId: 'notifications', perspective: 'main'}}).then(navItem => {
    //   if (navItem.length === 0) {
    //     MainNavigation.create({
    //       text: 'mod-notifications:NOTIFICATIONS',
    //       perspective: 'main',
    //       iconCls: 'x-fa fa-users',
    //       // rowCls: 'nav-tree-badge nav-tree-badge-new',
    //       viewType: 'notifications',
    //       routeId: 'notifications',
    //       leaf: true,
    //     }).then(newNavItem => {
    //       log.info(newNavItem);
    //       done(null, true);
    //     }).catch(err => {
    //       log.error(err);
    //       done(null, false);
    //     });
    //   } else {
    //     done(null, true);
    //   }
    // }).catch(err => {
    //   log.error(err);
    //   done(null, true);
    // });
  }
  getnotifications(bootClass, req, type, start, limit, next) {
    let me = this;
    me.roles.find().then((roles) => {
      if (_.find(roles, {name: 'Administrator'}) === undefined && type === NotificationTypes.ADMIN) {
        return next(new Error('Not Permitted'));
      }
      let app = bootClass.app;
      let registry = app.registry;
      let Notification = registry.getModelByType('Notification');
      let where = {type: type};
      if (type !== NotificationTypes.ADMIN) {
        if (me.accountId && me.accountId !== '' && me.accountId !== null) {
          where.or = [{userId: me.id}, {accountId: me.accountId}];
        } else {
          where.userId = me.id;
        }
      }
      // log.info('Query:', {where: where, limit: limit, skip: start});
      async.series({
        total: (cb) => {
          Notification.count(where, cb);
        },
        data: (cb) => {
          Notification.find({where: where, limit: limit, skip: start, order: 'createdOn DESC'}, cb);
        }
      }, (err, results) => {
        if (err) {
          next(err);
        } else {
          next(null, results.total, results.data);
        }
      });
    }).catch((err) => {
      console.log(err);
    });
  }
}
module.exports = Boot;
