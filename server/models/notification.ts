import {LoopBackApplication, Role, PersistedModel} from "loopback";
import {Socket} from "socket.io";
let log = require('@dosarrest/loopback-component-logger')('extlb-notifications/server/models/Notification');

export enum NotificationTypes {
  MAIN = 0,
  ADMIN = 1,
}
export enum NotificationActions {
  NONE = 0,
  REDIRECT_HASH = 1,
  REDIRECT_URL = 2,
}

export interface NotificationInterface extends PersistedModel {
  id: string;
  content: string;
  action: NotificationActions;
  options: any,
  type: NotificationTypes;
  createdOn: Date;
}
export interface Notification extends PersistedModel {
}

class NotificationModel {
  app: LoopBackApplication;
  model: Notification;
  constructor(model: Notification) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then((events: any) => {
      return me.setupModel();
    }).then((methods: any) => {
      return me.setupMethods();
    }).catch((err: Error) => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        log.info('Attached Notification Model to Application');
      });
      (me.model as any).observe('after save', (ctx: any, next: Function) => {
        if (ctx.isNewInstance) {
          let socket: Socket = (me.app as any).io;
          let authOptions = me.app.get('auth');
          if (authOptions.enabled === true) {
            if (ctx.instance.type === NotificationTypes.ADMIN) {
              socket.to('AdminNotifications')
                .emit('new-AdminNotification', ctx.instance);
            } else {
              if (ctx.instance.accountId === '') {
                socket.to(`user-${ctx.instance.userId}`)
                  .to(`user-${ctx.instance.userId}-Notifications`)
                  // .to('Notifications')
                  .emit('new-Notification', ctx.instance);
              } else {
                socket.to(`account-${ctx.instance.accountId}`)
                  .to(`account-${ctx.instance.accountId}-Notifications`)
                  // .to('Notifications')
                  .emit('new-Notification', ctx.instance);
              }
            }
          } else {
            socket.to('Notifications').emit('new-Notification', ctx.instance);
          }
          next();
        } else {
          next();
        }
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}

export default function (Notification: Notification) {
  new NotificationModel(Notification);
};
