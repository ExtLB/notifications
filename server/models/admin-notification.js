"use strict";
exports.__esModule = true;
var log = require('@dosarrest/loopback-component-logger')('senchaloop-perspective-admin/server/models/AdminNotification');
var AdminNotificationModel = (function () {
    function AdminNotificationModel(model) {
        var me = this;
        me.model = model;
        me.setup();
    }
    AdminNotificationModel.prototype.setup = function () {
        var me = this;
        return me.setupEvents().then(function (events) {
            return me.setupModel();
        }).then(function (methods) {
            return me.setupMethods();
        })["catch"](function (err) {
            log.error(err);
        });
    };
    AdminNotificationModel.prototype.setupModel = function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            resolve(true);
        });
    };
    AdminNotificationModel.prototype.setupEvents = function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            me.model.on('attached', function (app) {
                me.app = app;
                log.info('Attached AdminNotification Model to Application');
            });
            resolve(true);
        });
    };
    AdminNotificationModel.prototype.setupMethods = function () {
        var me = this;
        return new Promise(function (resolve) {
            resolve(true);
        });
    };
    return AdminNotificationModel;
}());
function default_1(AdminNotification) {
    new AdminNotificationModel(AdminNotification);
}
exports["default"] = default_1;
;
//# sourceMappingURL=admin-notification.js.map