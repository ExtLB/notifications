import { PersistedModel } from "loopback";
export interface AdminNotificationInterface extends PersistedModel {
    id: string;
    content: string;
    action: string;
    type: number;
    status: number;
    createdOn: Date;
}
export interface AdminNotification extends PersistedModel {
}
export default function (AdminNotification: AdminNotification): void;
