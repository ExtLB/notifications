import { PersistedModel } from "loopback";
export declare enum NotificationTypes {
    MAIN = 0,
    ADMIN = 1
}
export declare enum NotificationActions {
    NONE = 0,
    REDIRECT_HASH = 1,
    REDIRECT_URL = 2
}
export interface NotificationInterface extends PersistedModel {
    id: string;
    content: string;
    action: NotificationActions;
    options: any;
    type: NotificationTypes;
    createdOn: Date;
}
export interface Notification extends PersistedModel {
}
export default function (Notification: Notification): void;
