"use strict";
exports.__esModule = true;
var log = require('@dosarrest/loopback-component-logger')('extlb-notifications/server/models/Notification');
var NotificationTypes;
(function (NotificationTypes) {
    NotificationTypes[NotificationTypes["MAIN"] = 0] = "MAIN";
    NotificationTypes[NotificationTypes["ADMIN"] = 1] = "ADMIN";
})(NotificationTypes = exports.NotificationTypes || (exports.NotificationTypes = {}));
var NotificationActions;
(function (NotificationActions) {
    NotificationActions[NotificationActions["NONE"] = 0] = "NONE";
    NotificationActions[NotificationActions["REDIRECT_HASH"] = 1] = "REDIRECT_HASH";
    NotificationActions[NotificationActions["REDIRECT_URL"] = 2] = "REDIRECT_URL";
})(NotificationActions = exports.NotificationActions || (exports.NotificationActions = {}));
var NotificationModel = (function () {
    function NotificationModel(model) {
        var me = this;
        me.model = model;
        me.setup();
    }
    NotificationModel.prototype.setup = function () {
        var me = this;
        return me.setupEvents().then(function (events) {
            return me.setupModel();
        }).then(function (methods) {
            return me.setupMethods();
        })["catch"](function (err) {
            log.error(err);
        });
    };
    NotificationModel.prototype.setupModel = function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            resolve(true);
        });
    };
    NotificationModel.prototype.setupEvents = function () {
        var me = this;
        return new Promise(function (resolve, reject) {
            me.model.on('attached', function (app) {
                me.app = app;
                log.info('Attached Notification Model to Application');
            });
            me.model.observe('after save', function (ctx, next) {
                if (ctx.isNewInstance) {
                    var socket = me.app.io;
                    var authOptions = me.app.get('auth');
                    if (authOptions.enabled === true) {
                        if (ctx.instance.type === NotificationTypes.ADMIN) {
                            socket.to('AdminNotifications')
                                .emit('new-AdminNotification', ctx.instance);
                        }
                        else {
                            if (ctx.instance.accountId === '') {
                                socket.to("user-" + ctx.instance.userId)
                                    .to("user-" + ctx.instance.userId + "-Notifications")
                                    .emit('new-Notification', ctx.instance);
                            }
                            else {
                                socket.to("account-" + ctx.instance.accountId)
                                    .to("account-" + ctx.instance.accountId + "-Notifications")
                                    .emit('new-Notification', ctx.instance);
                            }
                        }
                    }
                    else {
                        socket.to('Notifications').emit('new-Notification', ctx.instance);
                    }
                    next();
                }
                else {
                    next();
                }
            });
            resolve(true);
        });
    };
    NotificationModel.prototype.setupMethods = function () {
        var me = this;
        return new Promise(function (resolve) {
            resolve(true);
        });
    };
    return NotificationModel;
}());
function default_1(Notification) {
    new NotificationModel(Notification);
}
exports["default"] = default_1;
;
//# sourceMappingURL=notification.js.map