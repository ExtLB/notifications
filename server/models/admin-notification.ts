import {LoopBackApplication, Role, PersistedModel} from "loopback";
let log = require('@dosarrest/loopback-component-logger')('senchaloop-perspective-admin/server/models/AdminNotification');

export interface AdminNotificationInterface extends PersistedModel {
  id: string;
  content: string;
  action: string;
  type: number;
  status: number;
  createdOn: Date;
}
export interface AdminNotification extends PersistedModel {
}

class AdminNotificationModel {
  app: LoopBackApplication;
  model: AdminNotification;
  constructor(model: AdminNotification) {
    let me = this;
    me.model = model;
    me.setup();
  }
  setup() {
    let me = this;
    return me.setupEvents().then((events: any) => {
      return me.setupModel();
    }).then((methods: any) => {
      return me.setupMethods();
    }).catch((err: Error) => {
      log.error(err);
    });
  }
  setupModel() {
    let me = this;
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
  setupEvents() {
    let me = this;
    return new Promise((resolve, reject) => {
      (me.model as any).on('attached', (app: LoopBackApplication) => {
        me.app = app;
        log.info('Attached AdminNotification Model to Application');
      });
      resolve(true);
    });
  }
  setupMethods() {
    let me = this;
    return new Promise((resolve) => {
      resolve(true);
    });
  }
}

export default function (AdminNotification: AdminNotification) {
  new AdminNotificationModel(AdminNotification);
};
